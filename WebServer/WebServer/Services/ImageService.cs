﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebServer.Services
{
    public static class ImageService
    {
        private static string _root = "~/App_Data/images/";

        public static void SaveImage(string name, string content)
        {
            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath(_root + name), content);
        }

        public static void DeleteImage(string name)
        {
            File.Delete(System.Web.HttpContext.Current.Server.MapPath(_root + name));
        }

        public static string GetImage(string name)
        {
            return File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(_root + name));
        }

        public static bool Exist(string name)
        {
            return File.Exists(System.Web.HttpContext.Current.Server.MapPath(_root + name));
        }
    }
}