﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServer.Models;

namespace WebServer.Services
{
    public class DataService
    {
        private RedBook _contexDb;

        public DataService(RedBook context)
        {
            _contexDb = context;
        }

        public static ICollection<WebImage> GetWebImages(ICollection<Image> inputImages)
        {
            ICollection<WebImage> outputWebImages = new List<WebImage>();
            foreach (Image image in inputImages)
                outputWebImages.Add(WebImage.CreateFromImage(image));
            return outputWebImages;
        }

        public ICollection<Image> GetValidImages(ICollection<Image> inputImages)
        {
            ICollection<Image> outputImages = new List<Image>();
            foreach (Image image in inputImages)
            {
                Image findedImage = _contexDb.Images.First(x => x.Id == image.Id);
                if (findedImage != null) outputImages.Add(findedImage);
            }
            return outputImages;
        }

        public Image GetUpdatedImage(WebImage image)
        {
            Image updatedImage = _contexDb.Images.Find(image.Id);
            if (updatedImage != null)
            {
                updatedImage.Name = image.Name;
                updatedImage.ImageType = GetValidImageType(image.ImageType);
                updatedImage.Path = image.Path;
            }
            return updatedImage;
        }

        public Image CreateImage(WebImage webImage)
        {
            Image image = Image.GetImage(webImage.Name, webImage.Path, GetValidImageType(webImage.ImageType));
            return image;
        }

        public ImageType GetValidImageType(ImageType imageType)
        {
            return _contexDb.ImageTypes.Find(imageType.Id);
        }

        public AnimalType GetValidAnimalType(AnimalType inputAnimalType)
        {
            return _contexDb.AnimalTypes.Find(inputAnimalType.Id);
        }

        public Animal GetUpdatedAnimal(Animal animal)
        {
            Animal updatedAnimal = _contexDb.Animals.Find(animal.Id);
            if (updatedAnimal != null)
            {
                updatedAnimal.Name = animal.Name;
                updatedAnimal.ShortContent = animal.ShortContent;
                updatedAnimal.Content = animal.Content;
                updatedAnimal.Images = GetValidImages(animal.Images);
                updatedAnimal.AnimalType = GetValidAnimalType(animal.AnimalType);
            }
            return updatedAnimal;
        }
    }
}