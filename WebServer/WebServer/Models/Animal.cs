﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebServer.Models
{
    [JsonObject]
    public class Animal
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortContent { get; set; }
        public string Content { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public int AnimalTypeId { get; set; }
        public virtual AnimalType AnimalType { get; set; }

        public Animal()
        {
            Images = new List<Image>();
        }

        public static Animal GetAnimal(string name, string shortContent, string content, ICollection<Image> images,
            AnimalType animalType, int id = 0) {
            Animal outputAnimal = new Animal() {
                Name = name,
                Content = content,
                ShortContent = shortContent,
                Images = images,
                AnimalType = animalType
            };
            if (id != 0) outputAnimal.Id = id;
            return outputAnimal;
        }
    }
}