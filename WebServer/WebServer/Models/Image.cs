﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WebServer.Services;

namespace WebServer.Models
{
    [JsonObject]
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int ImageTypeId { get; set; }
        public virtual ImageType ImageType { get; set; }
        public int? AnimalId { get; set; }
        [JsonIgnore]
        public virtual Animal Animal { get; set; }

        public static Image GetImage(string name, string path, ImageType type)
        {
            Image outputImage = new Image()
            {
                Name = name,
                Path = path,
                ImageType = type
            };
            return outputImage;
        }
    }

    public class WebImage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Picture { get; set; }
        public ImageType ImageType { get; set; }
        public static WebImage CreateFromImage(Image image)
        {
            WebImage webImage = new WebImage()
            {
                Id = image.Id,
                Name = image.Name,
                Path = image.Path,
                Picture = ImageService.GetImage(image.Path),
                ImageType = image.ImageType
            };
            return webImage;
        }
    }
}