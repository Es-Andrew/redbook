﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebServer.Models
{
    [JsonObject]
    public class AnimalType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Animal> Animals { get; set; }

        public AnimalType()
        {
            Animals = new List<Animal>();
        }
    }
}