namespace WebServer.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class RedBook : DbContext
    {
        public RedBook()
            : base("name=RedBook")
        {

        }

        public virtual DbSet<Animal> Animals { get; set; }
        public virtual DbSet<AnimalType> AnimalTypes { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<ImageType> ImageTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Image>()
            //    .HasRequired(i => i.ImageType)
            //    .WithMany(i => i.Images);

            //modelBuilder.Entity<Image>()
            //    .HasRequired(i => i.Animal)
            //    .WithMany(i => i.Images);

            //modelBuilder.Entity<Animal>()
            //    .HasRequired(a => a.AnimalType)
            //    .WithMany(a => a.Animals);
        }

        public static RedBook Create()
        {
            return new RedBook();
        }
    }
}