﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Newtonsoft.Json;

namespace WebServer.Models
{
    [JsonObject]
    public class ImageType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Image> Images { get; set; }

        public ImageType()
        {
            Images = new List<Image>();
        }
    }
}