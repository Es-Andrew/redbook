namespace WebServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                        ShortContent = c.String(maxLength: 4000),
                        Content = c.String(maxLength: 4000),
                        AnimalTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AnimalTypes", t => t.AnimalTypeId, cascadeDelete: true)
                .Index(t => t.AnimalTypeId);
            
            CreateTable(
                "dbo.AnimalTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                        Path = c.String(maxLength: 4000),
                        ImageTypeId = c.Int(nullable: false),
                        AnimalId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Animals", t => t.AnimalId)
                .ForeignKey("dbo.ImageTypes", t => t.ImageTypeId, cascadeDelete: true)
                .Index(t => t.ImageTypeId)
                .Index(t => t.AnimalId);
            
            CreateTable(
                "dbo.ImageTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "ImageTypeId", "dbo.ImageTypes");
            DropForeignKey("dbo.Images", "AnimalId", "dbo.Animals");
            DropForeignKey("dbo.Animals", "AnimalTypeId", "dbo.AnimalTypes");
            DropIndex("dbo.Images", new[] { "AnimalId" });
            DropIndex("dbo.Images", new[] { "ImageTypeId" });
            DropIndex("dbo.Animals", new[] { "AnimalTypeId" });
            DropTable("dbo.ImageTypes");
            DropTable("dbo.Images");
            DropTable("dbo.AnimalTypes");
            DropTable("dbo.Animals");
        }
    }
}
