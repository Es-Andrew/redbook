using System.Collections.Generic;
using WebServer.Models;

namespace WebServer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebServer.Models.RedBook>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(WebServer.Models.RedBook context)
        {
            AnimalType animalType01 = new AnimalType() {Name = "�������������"};
            AnimalType animalType02 = new AnimalType() {Name = "���������" };
            AnimalType animalType03 = new AnimalType() { Name = "��������������" };
            ImageType imageType01 = new ImageType() {Name = "�������"};
            ImageType imageType02 = new ImageType() { Name = "��������������" };

            context.AnimalTypes.AddRange(new List<AnimalType>()
            {
                animalType01,
                animalType02,
                animalType03
            });
            context.ImageTypes.Add(imageType01);
            context.ImageTypes.Add(imageType02);

            context.SaveChanges();

            Animal dog = new Animal()
            {
                AnimalType = animalType01,
                Name = "������",
                Content = "������� ������",
                ShortContent = "����� ������� ������..."
            };

            Animal fox = new Animal()
            {
                AnimalType = animalType01,
                Name = "����",
                Content = "�������� ����",
                ShortContent = "����� �������� ����"
            };

            context.Animals.Add(dog);
            context.Animals.Add(fox);

            context.SaveChanges();
        }
    }
}
