﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebServer.Models;
using WebServer.Services;

namespace WebServer.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ImagesController : ApiController
    {
        private RedBook _contexDb = new RedBook();
        
        [AllowAnonymous]
        public IHttpActionResult GetImages()
        {
            return Json(DataService.GetWebImages(_contexDb.Images.ToList()));
        }
        
        [AllowAnonymous]
        [ResponseType(typeof(Image))]
        public IHttpActionResult GetImage(int id)
        {
            Image image = _contexDb.Images.Find(id);
            if (image == null)
            {
                return NotFound();
            }

            return Ok(WebImage.CreateFromImage(image));
        }

        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImage(int id, WebImage image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != image.Id)
            {
                return BadRequest();
            }

            string oldPath = _contexDb.Images.Find(id).Path;

            DataService dataService = new DataService(_contexDb);
            dataService.GetUpdatedImage(image);

            try
            {
                _contexDb.SaveChanges();
                ImageService.DeleteImage(oldPath);
                ImageService.SaveImage(image.Path, image.Picture);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [AllowAnonymous]
        [ResponseType(typeof(Image))]
        public IHttpActionResult PostImage(WebImage image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DataService dataService = new DataService(_contexDb);
            Image addImage = Image.GetImage(image.Name, image.Path, dataService.GetValidImageType(image.ImageType));
            _contexDb.Images.Add(addImage);
            _contexDb.SaveChanges();
            ImageService.SaveImage(image.Path, image.Picture);
            return CreatedAtRoute("DefaultApi", new { id = addImage.Id }, addImage);
        }

        [AllowAnonymous]
        [ResponseType(typeof(Image))]
        public IHttpActionResult DeleteImage(int id)
        {
            Image image = _contexDb.Images.Find(id);
            if (image == null)
            {
                return NotFound();
            }

            _contexDb.Images.Remove(image);
            _contexDb.SaveChanges();
            ImageService.DeleteImage(image.Path);
            return Ok(image);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _contexDb.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ImageExists(int id)
        {
            return _contexDb.Images.Count(e => e.Id == id) > 0;
        }
    }
}