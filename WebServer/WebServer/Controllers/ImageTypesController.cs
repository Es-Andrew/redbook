﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebServer.Models;

namespace WebServer.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ImageTypesController : ApiController
    {
        private RedBook _contexDb = new RedBook();
        
        [AllowAnonymous]
        public IHttpActionResult GetImageTypes()
        {
            return Json(_contexDb.ImageTypes);
        }
        
        [AllowAnonymous]
        [ResponseType(typeof(ImageType))]
        public IHttpActionResult GetImageType(int id)
        {
            ImageType imageType = _contexDb.ImageTypes.Find(id);
            if (imageType == null)
            {
                return NotFound();
            }

            return Ok(imageType);
        }

        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImageType(int id, ImageType imageType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imageType.Id)
            {
                return BadRequest();
            }

            _contexDb.Entry(imageType).State = EntityState.Modified;

            try
            {
                _contexDb.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImageTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [AllowAnonymous]
        [ResponseType(typeof(ImageType))]
        public IHttpActionResult PostImageType(ImageType imageType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _contexDb.ImageTypes.Add(imageType);
            _contexDb.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = imageType.Id }, imageType);
        }

        [AllowAnonymous]
        [ResponseType(typeof(ImageType))]
        public IHttpActionResult DeleteImageType(int id)
        {
            ImageType imageType = _contexDb.ImageTypes.Find(id);
            if (imageType == null)
            {
                return NotFound();
            }

            _contexDb.ImageTypes.Remove(imageType);
            _contexDb.SaveChanges();

            return Ok(imageType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _contexDb.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ImageTypeExists(int id)
        {
            return _contexDb.ImageTypes.Count(e => e.Id == id) > 0;
        }
    }
}