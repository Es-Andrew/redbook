﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Cors;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Newtonsoft.Json;
using WebServer.Models;
using WebServer.Services;

namespace WebServer.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class AnimalsController : ApiController
    {
        private RedBook _contexDb = new RedBook();

        private DataService dataService;
        
        [AllowAnonymous]
        public IHttpActionResult GetAnimals()
        {
            Debug.WriteLine(_contexDb.Animals.Select(x => x.Name == "Dog").Count());
            return Json(_contexDb.Animals);
        }

        [AllowAnonymous]
        [ResponseType(typeof(Animal))]
        public IHttpActionResult GetAnimal(int id)
        {
            Animal animal = _contexDb.Animals.Find(id);
            if (animal == null)
            {
                return NotFound();
            }

            return Ok(animal);
        }

        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnimal(int id, Animal animal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != animal.Id)
            {
                return BadRequest();
            }

            dataService = new DataService(_contexDb);
            
            dataService.GetUpdatedAnimal(animal);

            try
            {
                _contexDb.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        
        [AllowAnonymous]
        [ResponseType(typeof(Animal))]
        public IHttpActionResult PostAnimal(Animal animal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            dataService = new DataService(_contexDb);

            Animal addAnimal = Animal.GetAnimal(animal.Name, animal.ShortContent, animal.Content,
                dataService.GetValidImages(animal.Images), dataService.GetValidAnimalType(animal.AnimalType));

            _contexDb.Animals.Add(addAnimal);
            _contexDb.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = addAnimal.Id }, addAnimal);
        }
        
        [AllowAnonymous]
        [ResponseType(typeof(Animal))]
        public IHttpActionResult DeleteAnimal(int id)
        {
            Animal animal = _contexDb.Animals.Find(id);
            if (animal == null)
            {
                return NotFound();
            }

            animal.Images.Clear();

            _contexDb.Animals.Remove(animal);
            _contexDb.SaveChanges();

            return Ok(animal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _contexDb.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnimalExists(int id)
        {
            return _contexDb.Animals.Count(e => e.Id == id) > 0;
        }
    }
}