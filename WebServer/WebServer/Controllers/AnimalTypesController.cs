﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebServer.Models;

namespace WebServer.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class AnimalTypesController : ApiController
    {
        private RedBook _contexDb = new RedBook();
        
        [AllowAnonymous]
        public IHttpActionResult GetAnimalTypes()
        {
            return Json(_contexDb.AnimalTypes);
        }
        
        [AllowAnonymous]
        [ResponseType(typeof(AnimalType))]
        public IHttpActionResult GetAnimalType(int id)
        {
            AnimalType animalType = _contexDb.AnimalTypes.Find(id);
            if (animalType == null)
            {
                return NotFound();
            }

            return Ok(animalType);
        }

        [AllowAnonymous]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnimalType(int id, AnimalType animalType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != animalType.Id)
            {
                return BadRequest();
            }

            _contexDb.Entry(animalType).State = EntityState.Modified;

            try
            {
                _contexDb.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [AllowAnonymous]
        [ResponseType(typeof(AnimalType))]
        public IHttpActionResult PostAnimalType(AnimalType animalType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _contexDb.AnimalTypes.Add(animalType);
            _contexDb.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = animalType.Id }, animalType);
        }

        [AllowAnonymous]
        [ResponseType(typeof(AnimalType))]
        public IHttpActionResult DeleteAnimalType(int id)
        {
            AnimalType animalType = _contexDb.AnimalTypes.Find(id);
            if (animalType == null)
            {
                return NotFound();
            }

            _contexDb.AnimalTypes.Remove(animalType);
            _contexDb.SaveChanges();

            return Ok(animalType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _contexDb.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnimalTypeExists(int id)
        {
            return _contexDb.AnimalTypes.Count(e => e.Id == id) > 0;
        }
    }
}