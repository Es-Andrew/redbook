import 'dart:collection';

import 'package:dartson/dartson.dart';

enum Content { Animal, AnimalType, Image }

enum ControlState { Add, Change }

class Control {
  static String GetType(ControlState state) {
    switch (state) {
      case ControlState.Add:
        return "Add";
      case ControlState.Change:
      return "Change";
      default: return "Error";
    }
  }
}

@Entity()
class ImageType {
  int id;
  String name;

  ImageType(this.id, this.name) {
  }

  factory ImageType.FromJson(Map<String, dynamic> imageType) =>
      new ImageType(imageType["Id"], imageType["Name"]);
}

@Entity()
class AnimalType {
  int id;
  String name;

  AnimalType(this.id, this.name) {
  }

  factory AnimalType.FromJson(Map<String, dynamic> animalType) =>
      new AnimalType(animalType["Id"], animalType["Name"]);
}

@Entity()
class Image {
  int id;
  String name;
  String path;
  String picture;
  ImageType imageType;

  Image(this.id, this.name, this.path, this.imageType, this.picture) {
  }

  factory Image.FromJson(Map<String, dynamic> image) => new Image(
      image["Id"],
      image["Name"],
      image["Path"],
      new ImageType.FromJson(image["ImageType"]),
      image["Picture"]);
}

@Entity()
class Animal {
  int id;
  String name;
  String shortContent;
  String content;
  AnimalType animalType;
  List<Image> images;

  Animal(this.id, this.name, this.shortContent, this.content, this.animalType,
      this.images) {
  }

  factory Animal.FromJson(Map<String, dynamic> animal) => new Animal(
      animal["Id"],
      animal["Name"],
      animal["ShortContent"],
      animal["Content"],
      new AnimalType.FromJson(animal["AnimalType"]),
      _GetImagesFromJson(animal["Images"]));

  static List<Image> _GetImagesFromJson(List data) {
    List<Image> images = new List<Image>();
    for (LinkedHashMap map in data) images.add(new Image.FromJson(map));
    return images;
  }
}
