// Copyright (c) 2017, Es-Andrew. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'components/nav_bar/nav_bar.dart';
import 'components/info_bar/info_bar.dart';
import 'components/table_set/table_set.dart';
import 'components/animal_table/animal_table.dart';
import 'components/animal_type_table/animal_type_table.dart';
import 'components/image_viewer/image_viewer.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/services/navigation.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [
    materialDirectives,
    NavBar,
    InfoBar,
    TableSet,
    AnimalTable,
    AnimalTypeTable,
    ImageViewer
  ],
  providers: const [materialProviders, DataService, Navigation],
  encapsulation: ViewEncapsulation.Emulated,
)
class AppComponent implements OnInit {
  DataService data;
  Navigation navigation;
  String page;

  AppComponent() {
    data = new DataService();
  }

  String validationParameter;

  Content state = Content.Animal;
  String content = "Животные";

  void onValidationParameterChange(String value) {
    validationParameter = value;
  }

  void onPageChange(Navigation inputNavigation) {
    navigation = inputNavigation;
    page = GetPageType();
    content = navigation.GetPageName();
  }

  String GetPageType() {
    switch (navigation.currentPage) {
      case Page.Animal:
        return "Animal";
      case Page.AnimalType:
        return "AnimalType";
      case Page.Image:
        return "Image";
      default:
        return "Other";
    }
  }

  @override
  ngOnInit() {
    DataService.UpdateData();
    navigation = new Navigation();
    page = "Animal";
  }
}
