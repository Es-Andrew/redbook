import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'animal-type-change',
  styleUrls: const ['animal_type_change.css'],
  templateUrl: 'animal_type_change.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalTypeChange implements AfterViewInit {
  static DataService data = new DataService();

  ElementRef _elementRef;

  AnimalTypeChange(this._elementRef);

  @Input("selectedAnimalType")
  AnimalType animalType;

  void Update() {
    if (_elementRef.nativeElement.querySelector("#tab").getAttribute("state") == "open")
      CloseTab();
    else
      OpenTab();
  }

  void CloseTab() {
    _elementRef.nativeElement.querySelector("#tab").setAttribute("state", "close");
  }

  void OpenTab() {
    _elementRef.nativeElement.querySelector("#tab").setAttribute("state", "open");
  }

  void UpdateAnimal() {
    DataService.PutAnimalType(animalType);
  }

  @override
  ngAfterViewInit() {
    // TODO: implement ngAfterViewInit
  }
}
