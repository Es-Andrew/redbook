import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/navigation.dart';

import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'nav-bar',
  styleUrls: const ['nav_bar.css'],
  templateUrl: 'nav_bar.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, Navigation],
  encapsulation: ViewEncapsulation.Emulated,
)
class NavBar implements OnInit {
  @Input()
  Navigation navigation;

  @Output("selectedPage")
  EventEmitter<Navigation> selectedPage = new EventEmitter<Navigation>();

  void navigateToAnimalPage() {
    navigation.SetAnimalPage();
    onPageChange();
  }

  void navigateToAnimalTypePage() {
    navigation.SetAnimalTypePage();
    onPageChange();
  }

  void navigateToImagePage() {
    navigation.SetImagePage();
    onPageChange();
  }

  void onPageChange() {
    selectedPage.emit(navigation);
  }

  @override
  ngOnInit() {
  }
}
