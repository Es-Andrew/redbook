import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

import 'package:AdminApp/components/image_selector/image_selector.dart';
import '..//animal_type_select/animal_type_select.dart';

@Component(
  selector: 'animal-add',
  styleUrls: const ['animal_add.css'],
  templateUrl: 'animal_add.html',
  directives: const [materialDirectives, AnimalTypeSelect, ImageSelector],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalAdd implements OnInit, AfterViewInit {
  static DataService data = new DataService();

  @Input()
  List<Image> images;

  ElementRef _elementRef;

  AnimalAdd(this._elementRef);

  @ViewChild("animalType")
  AnimalTypeSelect animalType;

  static AnimalType selectedAnimalType = data.GetAnimalTypes().length > 0
      ? data.GetAnimalTypes().first
      : new AnimalType(1, "Млекопитающие");

  Animal addAnimal = new Animal(
      0,
      "Введите название",
      "Введете краткое описание",
      "Введите полное описание",
      selectedAnimalType,
      new List<Image>());

  void Update() {
    if (_elementRef.nativeElement.querySelector("#tab").getAttribute("state") ==
        "open")
      CloseTab();
    else
      OpenTab();
  }

  void CloseTab() {
    _elementRef.nativeElement
        .querySelector("#tab")
        .setAttribute("state", "close");
  }

  void OpenTab() {
    _elementRef.nativeElement
        .querySelector("#tab")
        .setAttribute("state", "open");
  }

  void AddAnimal() {
    DataService.PostAnimal(addAnimal);
  }

  void onUpdateImages(List<Image> images) {
    addAnimal.images = images;
    print(addAnimal.images);
  }

  @override
  ngAfterViewInit() {}

  @override
  ngOnInit() {
  }
}
