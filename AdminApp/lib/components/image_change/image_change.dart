import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/components/image_type_select/image_type_select.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'image-change',
  styleUrls: const ['image_change.css'],
  templateUrl: 'image_change.html',
  directives: const [materialDirectives, ImageTypeSelect],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class ImageChange implements OnInit, OnChanges {

  @Input()
  Image image;
  String imageLabel;

  Image recoveryImage;

  ElementRef _elementRef;
  ImageChange(this._elementRef);

  @ViewChild("imageType")
  ImageTypeSelect imageType;

  bool state;

  void open() {
    state = true;
    Element main = _elementRef.nativeElement.querySelector("#image-add");
    main.setAttribute("state", "open");
  }

  void close() {
    state = false;
    Element main = _elementRef.nativeElement.querySelector("#image-add");
    main.setAttribute("state", "close");
  }

  void onImageTypeUpdate(ImageType imageType) {
    image.imageType = imageType;
  }

  void onSelect() {
    Element loader = _elementRef.nativeElement.querySelector("#loader");
    loader.click();
  }

  void updateImageLabel(String content) {
    imageLabel = content;
  }

  void onChange(dynamic data) {
    FileReader reader = new FileReader();
    reader.onLoad.listen((event) {
      recoveryImage.path = image.path;
      recoveryImage.picture = image.picture;
      image.picture = reader.result;
      image.path = data.first.name;
      updateImageLabel(data.first.name);
    });
    reader.readAsDataUrl((data as FileList).first);
  }

  void onClear() {
    if(image != null && recoveryImage != null) {
      image.path = recoveryImage.path;
      image.picture = recoveryImage.picture;
    }
  }

  void onSubmit() {
    if (image.picture != "" &&
        image.picture != null &&
        image.imageType != null) {
      DataService.PutImage(image);
      close();
    }
  }

  @override
  ngOnInit() {
    state = false;
    imageLabel = "Выберите изображение";
    recoveryImage = DataService.GetDefaultImage();
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    // 
  }
}
