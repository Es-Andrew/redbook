import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

import 'package:AdminApp/components/image_selector/image_selector.dart';
import '..//animal_type_select/animal_type_select.dart';

@Component(
  selector: 'animal-change',
  styleUrls: const ['animal_change.css'],
  templateUrl: 'animal_change.html',
  directives: const [materialDirectives, AnimalTypeSelect, ImageSelector],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalChange implements AfterViewInit {
  static DataService data = new DataService();

  @Input()
  List<Image> images;

  ElementRef _elementRef;

  AnimalChange(this._elementRef);


  @ViewChild("animalType")
  AnimalTypeSelect animalType;

  @Input("selectedAnimal")
  Animal animal;

  void Update() {
    if (_elementRef.nativeElement.querySelector("#tab").getAttribute("state") == "open")
      CloseTab();
    else
      OpenTab();
  }

  void CloseTab() {
    _elementRef.nativeElement.querySelector("#tab").setAttribute("state", "close");
  }

  void OpenTab() {
    _elementRef.nativeElement.querySelector("#tab").setAttribute("state", "open");
  }

  void UpdateAnimal() {
    DataService.PutAnimal(animal);
  }

  void onUpdateImages(List<Image> images) {
    animal.images = images;
    print(animal.images);
  }

  @override
  ngAfterViewInit() {
    // TODO: implement ngAfterViewInit
  }
}
