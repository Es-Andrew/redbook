import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/components/thumbnail/thumbnail.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/services/pagination_service.dart';
import '../../models/input_models.dart';

@Component(
  selector: 'image-selector',
  styleUrls: const ['image_selector.css'],
  templateUrl: 'image_selector.html',
  directives: const [materialDirectives, Thumbnail],
  providers: const [materialProviders, DataService, PaginationService],
  encapsulation: ViewEncapsulation.Emulated,
)
class ImageSelector implements OnInit, OnChanges {
  DataService data = new DataService();

  @Input()
  List<Image> images;

  @Input()
  List<Image> selectedImages;

  Map<int, bool> selectState;

  @Output()
  EventEmitter<List<Image>> outSelectedImages = new EventEmitter<List<Image>>();

  PaginationService paginator;

  ImageSelector() {
    paginator = new PaginationService(images, 4, 5);
  }

  void onUpdate(Image image) {
    if (selectedImages == null) selectedImages = new List<Image>();
    if (!isSelected(image))
      onSelect(image);
    else
      onDeselect(image);
  }

  void onSelect(Image image) {
    selectedImages.add(image);
    outSelectedImages.emit(selectedImages);
  }

  void onDeselect(Image image) {
    if (selectedImages.contains(image)) selectedImages.remove(image);
    outSelectedImages.emit(selectedImages);
  }

  bool isSelected(Image image) {
    return selectedImages.contains(image);
  }

  @override
  ngOnInit() {}

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey("images")) images = changes["images"].currentValue;
    paginator.UpdateData(images);
  }
}
