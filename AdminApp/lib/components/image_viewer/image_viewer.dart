import 'dart:html';
import 'dart:math';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/components/thumbnail/thumbnail.dart';
import 'package:AdminApp/components/image_add/image_add.dart';
import 'package:AdminApp/components/image_change/image_change.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'image-viewer',
  styleUrls: const ['image_viewer.css'],
  templateUrl: 'image_viewer.html',
  directives: const [materialDirectives, Thumbnail, ImageAdd, ImageChange],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class ImageViewer implements OnInit, AfterViewChecked {
  DataService dataService;

  @Input("images")
  List<Image> images;

  ElementRef _elementRef;

  String mainKillingContent = r"data:image/jpg;base64";

  Image selectedImage;

  ImageViewer(this._elementRef) {
    dataService = new DataService();
  }

  void onResize() {
    Element slider = _elementRef.nativeElement.querySelector("#slider");
    Element viewport = _elementRef.nativeElement.querySelector("#viewport");
    Element image = _elementRef.nativeElement.querySelector("#selectedImage");
    viewport.style.top = (slider.offsetHeight).toString() + "px";
    viewport.style.height = (document.body.clientHeight -
                slider.offsetHeight -
                document.querySelector("#info-bar").clientHeight)
            .toString() +
        "px";
    if (image != null) {
      image.style.maxHeight = viewport.clientHeight.toString() + "px";
      image.style.maxWidth = viewport.clientWidth.toString() + "px";
    }
  }

  void onSelect(Image image) {
    selectedImage = image;
  }

  @override
  ngOnInit() {
    document.window.addEventListener("resize", (Event) => onResize());
  }

  @override
  ngAfterViewChecked() {
    onResize();
  }
}
