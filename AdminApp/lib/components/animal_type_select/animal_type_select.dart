import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'animal-type-select',
  styleUrls: const ['animal_type_select.css'],
  templateUrl: 'animal_type_select.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalTypeSelect implements OnInit, AfterViewInit {
  DataService data = new DataService();

  ElementRef _elementRef;

  AnimalTypeSelect(this._elementRef);

  @Output()
  AnimalType selectedAnimalType;

  void Update() {
    if (_elementRef.nativeElement.querySelector("#content").getAttribute("state") == "open")
      Close();
    else
      Open();
  }

  void Open() {
    _elementRef.nativeElement.querySelector("#content").setAttribute("state", "open");
    _elementRef.nativeElement.querySelector("#selectAnimalType").setAttribute("state", "open");
  }

  void Close() {
    _elementRef.nativeElement.querySelector("#content").setAttribute("state", "close");
    _elementRef.nativeElement.querySelector("#selectAnimalType").setAttribute("state", "close");
  }

  void Select(int id) {
    selectedAnimalType = data.GetAnimalTypes().firstWhere((x) => x.id == id);
  }

  @override
  ngOnInit() {
    selectedAnimalType = data.GetAnimalTypes().length > 0
        ? data.GetAnimalTypes().first
        : new AnimalType(1, "Млекопитающие");
  }

  @override
  ngAfterViewInit() {
    // TODO: implement ngAfterViewInit
  }
}
