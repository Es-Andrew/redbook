import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'image-type-select',
  styleUrls: const ['image_type_select.css'],
  templateUrl: 'image_type_select.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class ImageTypeSelect implements OnInit, AfterViewInit {
  DataService data = new DataService();

  ElementRef _elementRef;

  ImageTypeSelect(this._elementRef);

  @Input()
  ImageType selectedImageType;

  @Output("selectedImageType")
  EventEmitter<ImageType> imageType = new EventEmitter<ImageType>(); 

  void Update() {
    if (_elementRef.nativeElement.querySelector("#content").getAttribute("state") == "open")
      Close();
    else
      Open();
  }

  void Open() {
    _elementRef.nativeElement.querySelector("#content").setAttribute("state", "open");
    _elementRef.nativeElement.querySelector("#selectImageType").setAttribute("state", "open");
  }

  void Close() {
    _elementRef.nativeElement.querySelector("#content").setAttribute("state", "close");
    _elementRef.nativeElement.querySelector("#selectImageType").setAttribute("state", "close");
  }

  void Select(int id) {
    selectedImageType = data.GetImageTypes().firstWhere((x) => x.id == id);
    imageType.emit(selectedImageType);
  }

  @override
   ngOnInit() {
  //   selectedImageType = data.GetImageTypes().length > 0
  //       ? data.GetImageTypes().first
  //       : new ImageType(1, "Основное");
  }

  @override
  ngAfterViewInit() {
    // TODO: implement ngAfterViewInit
  }
}
