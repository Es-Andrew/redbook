import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/services/validation_service.dart';
import 'package:AdminApp/services/pagination_service.dart';
import '../../models/input_models.dart';

import 'package:AdminApp/components/base/table.dart';
import 'package:AdminApp/components/animal_type_add/animal_type_add.dart';
import 'package:AdminApp/components/animal_type_change/animal_type_change.dart';

@Component(
  selector: 'animal-type-table',
  styleUrls: const ['animal_type_table.css'],
  templateUrl: 'animal_type_table.html',
  directives: const [materialDirectives, AnimalTypeAdd, AnimalTypeChange],
  providers: const [materialProviders, DataService, AnimalTypeValidationService, PaginationService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalTypeTable extends Table implements OnInit, OnChanges {
  DataService data = new DataService();

  @Input()
  List<AnimalType> animalTypes;

  @Input()
  String validationParameter;

  AnimalTypeValidationService validationService;
  PaginationService paginator;

  @ViewChild("animalTypeAdd")
  AnimalTypeAdd animalTypeAdd;

  @ViewChild("animalTypeChange")
  AnimalTypeChange animalTypeChange;

  AnimalType selectedAnimalType;

  AnimalTypeTable() {
    validationService = new AnimalTypeValidationService();
    paginator = new PaginationService(validationService.getValideData(animalTypes, validationParameter), 8, 5);
  }

  @override
  void Add() {
    animalTypeAdd.OpenTab();
  }

  void change(AnimalType animalType) {
    selectedAnimalType = animalType;
    animalTypeChange.OpenTab();
  }

  void delete(AnimalType animalType) {
    DataService.DeleteAnimalType(animalType);
  }

  @override
  ngOnInit() {
    selectedAnimalType =
        animalTypes.length > 0 ? animalTypes.first : DataService.GetDefaultAnimalType();
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    if(changes.containsKey("animalTypes")) animalTypes = changes["animalTypes"].currentValue;
    if(changes.containsKey("validationParameter")) validationParameter = changes["validationParameter"].currentValue;
    paginator.UpdateData(validationService.getValideData(animalTypes, validationParameter));
  }
}
