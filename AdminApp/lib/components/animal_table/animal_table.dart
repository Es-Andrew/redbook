import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/services/validation_service.dart';
import 'package:AdminApp/services/pagination_service.dart';
import '../../models/input_models.dart';

import 'package:AdminApp/components/base/table.dart';
import 'package:AdminApp/components/animal_add/animal_add.dart';
import 'package:AdminApp/components/animal_change/animal_change.dart';

@Component(
  selector: 'animal-table',
  styleUrls: const ['animal_table.css'],
  templateUrl: 'animal_table.html',
  directives: const [materialDirectives, AnimalAdd, AnimalChange],
  providers: const [materialProviders, DataService, AnimalValidationService, PaginationService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalTable extends Table implements OnInit, OnChanges {
  DataService data = new DataService();

  @Input()
  List<Animal> animals;

  @Input()
  List<Image> images;

  @Input()
  String validationParameter;

  AnimalValidationService validationService;
  PaginationService paginator;

  @ViewChild("animalAdd")
  AnimalAdd animalAdd;

  @ViewChild("animalChange")
  AnimalChange animalChange;

  Animal selectedAnimal;

  AnimalTable() {
    validationService = new AnimalValidationService();
    paginator = new PaginationService(validationService.getValideData(animals, validationParameter), 8, 5);
  }

  @override
  void Add() {
    animalAdd.OpenTab();
  }

  void change(Animal animal) {
    selectedAnimal = animal;
    animalChange.OpenTab();
  }

  void delete(Animal animal) {
    DataService.DeleteAnimal(animal);
  }

  @override
  ngOnInit() {
    selectedAnimal =
        animals.length > 0 ? animals.first : DataService.GetDefaultAnimal();
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    if(changes.containsKey("animals")) animals = changes["animals"].currentValue;
    if(changes.containsKey("images")) images = changes["images"].currentValue;
    if(changes.containsKey("validationParameter")) validationParameter = changes["validationParameter"].currentValue;
    paginator.UpdateData(validationService.getValideData(animals, validationParameter));
  }
}
