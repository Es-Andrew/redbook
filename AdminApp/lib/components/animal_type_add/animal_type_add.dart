import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'animal-type-add',
  styleUrls: const ['animal_type_add.css'],
  templateUrl: 'animal_type_add.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class AnimalTypeAdd implements AfterViewInit {
  static DataService data = new DataService();

  ElementRef _elementRef;

  AnimalTypeAdd(this._elementRef);

  AnimalType animalType = new AnimalType(0, "Введите название");

  void Update() {
    if (_elementRef.nativeElement.querySelector("#tab").getAttribute("state") ==
        "open")
      CloseTab();
    else
      OpenTab();
  }

  void CloseTab() {
    _elementRef.nativeElement
        .querySelector("#tab")
        .setAttribute("state", "close");
  }

  void OpenTab() {
    _elementRef.nativeElement
        .querySelector("#tab")
        .setAttribute("state", "open");
  }

  void AddAnimalType() {
    DataService.PostAnimalType(animalType);
  }

  @override
  ngAfterViewInit() {}
}
