import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';
import 'package:AdminApp/components/base/table.dart';
import 'package:AdminApp/components/animal_table/animal_table.dart';

@Component(
  selector: 'table-set',
  styleUrls: const ['table_set.css'],
  templateUrl: 'table_set.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, Table, AnimalTable],
  encapsulation: ViewEncapsulation.Emulated,
)
class TableSet implements OnInit{
  @Input()
  String title;

  Table table;

  String temp;

  @Output("validationParameter")
  EventEmitter<String> validationParameter = new EventEmitter<String>();

  @ContentChild("table")
  set animalTable(Table value) {
    print(value.runtimeType);
    table = value;
  }

  void openAddPanel() {
    table.Add();
  }

  void onChange(dynamic value) {
    validationParameter.emit(value as String);
  }

  @override
  ngOnInit() {
  }
}