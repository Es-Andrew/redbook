import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';

@Component(
  selector: 'info-bar',
  styleUrls: const ['info_bar.css'],
  templateUrl: 'info_bar.html',
  directives: const [materialDirectives],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class InfoBar implements OnInit{

  void onUpdate() {
    DataService.UpdateData();
  }

  @override
  ngOnInit() {
  }
}