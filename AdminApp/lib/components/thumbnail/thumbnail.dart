import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:AdminApp/services/data_service.dart';
import 'package:AdminApp/models/input_models.dart';

@Component(
  selector: 'thumbnail',
  styleUrls: const ['thumbnail.css'],
  templateUrl: 'thumbnail.html',
  directives: const [
    materialDirectives,
  ],
  providers: const [materialProviders, DataService],
  encapsulation: ViewEncapsulation.Emulated,
)
class Thumbnail implements OnInit, AfterViewChecked {
  ElementRef _elementRef;

  Thumbnail(this._elementRef);

  @Input("viewMode")
  bool viewMode;

  @Input("selectedImage")
  Image image;

  void setImageScale() {
    Element image = _elementRef.nativeElement.querySelector("#image");
    if (image != null) {
      num scaleX = 320 / image.clientWidth;
      num scaleY = 180 / image.clientHeight;
      if (scaleX == double.INFINITY) scaleX = 1;
      if (scaleY == double.INFINITY) scaleY = 1;
      // image.style.transform = "scale(" +
      //     (scaleX > scaleY ? scaleX.toString() : scaleY.toString()) +
      //     ")";
      //print(scaleX.toString() + " | " + scaleY.toString());
      //print(image.clientWidth.toString() + " | " + image.clientHeight.toString());
    }
  }

  void delete() {
    DataService.DeleteImage(image);
  }

  @override
  ngOnInit() {}

  @override
  ngAfterViewChecked() {
    setImageScale();
  }
}
