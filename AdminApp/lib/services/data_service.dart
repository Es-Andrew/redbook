import 'dart:async';

import 'package:AdminApp/models/input_models.dart';
import 'package:AdminApp/services/server.dart';

class DataService {
  static List<Animal> _animals = new List<Animal>();
  static List<AnimalType> _animalTypes = new List<AnimalType>();
  static List<Image> _images = new List<Image>();
  static List<ImageType> _imageTypes = new List<ImageType>();

  static final Server _server = new Server();

  DataService();

  List<Animal> GetAnimals() {
    return _animals;
  }

  List<AnimalType> GetAnimalTypes() {
    return _animalTypes;
  }

  List<Image> GetImages() {
    return _images;
  }

  List<ImageType> GetImageTypes() {
    return _imageTypes;
  }

  static Future UpdateData() async {
    _animals = await _server.GetAnimals();
    _animalTypes = await _server.GetAnimalTypes();
    _images = await _server.GetImages();
    _imageTypes = await _server.GetImageTypes();
  }

  static void PostAnimal(Animal animal) {
    _server.PostAnimal(animal);
    UpdateData();
  }

  static void PostAnimalType(AnimalType animalType) {
    _server.PostAnimalType(animalType);
    UpdateData();
  }

  static void PostImage(Image image) {
    _server.PostImage(image);
    UpdateData();
  }

  static void PutAnimal(Animal animal) {
    _server.PutAnimal(animal);
    UpdateData();
  }

  static void PutAnimalType(AnimalType animalType) {
    _server.PutAnimalType(animalType);
    UpdateData();
  }

  static void PutImage(Image image) {
    _server.PutImage(image);
    UpdateData();
  }

  static void DeleteAnimal(Animal animal) {
    _server.DeleteAnimal(animal);
    UpdateData();
  }

  static void DeleteAnimalType(AnimalType animalType) {
    _server.DeleteAnimalType(animalType);
    UpdateData();
  }

  static void DeleteImage(Image image) {
    _server.DeleteImage(image);
    UpdateData();
  }

  static Animal GetDefaultAnimal() {
    return new Animal(0, "", "", "", new AnimalType(0, ""), new List<Image>());
  }

  static AnimalType GetDefaultAnimalType() {
    return new AnimalType(0, "");
  }

  static Image GetDefaultImage() {
    return new Image(0, "", "", new ImageType(0, ""), "");
  }
}
