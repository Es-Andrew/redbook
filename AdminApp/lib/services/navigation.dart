enum Page { Animal, AnimalType, Image }

class Navigation {
  Page _currentPage = Page.Animal;

  void SetAnimalPage() {
    _currentPage = Page.Animal;
  }

  void SetAnimalTypePage() {
    _currentPage = Page.AnimalType;
  }

  void SetImagePage() {
    _currentPage = Page.Image;
  }

  Page get currentPage => _currentPage;

  String GetPageName() {
    switch (_currentPage) {
      case Page.Animal:
        return "Животные";
      case Page.AnimalType:
        return "Виды животных";
      case Page.Image:
        return "Изображения";
      default: return "Пустая";
    }
  }
}
