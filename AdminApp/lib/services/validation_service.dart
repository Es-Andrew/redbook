import 'package:AdminApp/models/input_models.dart';

abstract class ValidationService {
  bool isValide(dynamic dataItem, dynamic valideParameter);

  List<dynamic> getValideData(
      List<dynamic> inputData, dynamic valideParameter) {
    List<dynamic> outputData = new List<dynamic>();
    if (inputData != null)
      for (var i = 0; i < inputData.length; i++)
        if (isValide(inputData[i], valideParameter)) {
          outputData.add(inputData[i]);
          print(inputData[i].name);
        }
    return outputData;
  }
}

class AnimalValidationService extends ValidationService {
  RegExp _regExp;

  @override
  bool isValide(dynamic dataItem, dynamic valideParameter) {
    if (valideParameter == null || valideParameter == "") return true;
    if (isPropertyValide(dataItem.name, valideParameter as String)) return true;
    if (isPropertyValide(dataItem.content, valideParameter as String))
      return true;
    if (isPropertyValide(dataItem.shortContent, valideParameter as String))
      return true;
    if (isPropertyValide(dataItem.animalType.name, valideParameter as String))
      return true;
    if (isPropertyValide(
        dataItem.images.length.toString(), valideParameter as String))
      return true;
    return false;
  }

  bool isPropertyValide(String content, String valideParameter) {
    _regExp = new RegExp('' + valideParameter + '');
    return _regExp.hasMatch(content);
  }
}

class AnimalTypeValidationService extends ValidationService {
  RegExp _regExp;

  @override
  bool isValide(dynamic dataItem, dynamic valideParameter) {
    if (valideParameter == null || valideParameter == "") return true;
    if (isPropertyValide(dataItem.name, valideParameter as String)) return true;
    return false;
  }

  bool isPropertyValide(String content, String valideParameter) {
    _regExp = new RegExp('' + valideParameter + '');
    return _regExp.hasMatch(content);
  }
}

class ImageValidationService extends ValidationService {
  RegExp _regExp;

  @override
  bool isValide(dynamic dataItem, dynamic valideParameter) {
    if (valideParameter == null || valideParameter == "") return true;
    if (isPropertyValide(dataItem.name, valideParameter as String)) return true;
    if (isPropertyValide(dataItem.imageType.name, valideParameter as String))
      return true;
    return false;
  }

  bool isPropertyValide(String content, String valideParameter) {
    _regExp = new RegExp('' + valideParameter + '');
    return _regExp.hasMatch(content);
  }
}
