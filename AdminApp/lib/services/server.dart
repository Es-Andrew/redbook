import 'dart:async';
import 'dart:convert';
import 'dart:collection';

import 'package:json_god/json_god.dart';
import 'package:http/browser_client.dart';
import 'package:http/src/response.dart';

import 'package:AdminApp/models/input_models.dart';

class Server {
  Future<dynamic> GetJson(String url) async {
    Response response = await new BrowserClient().get(url);
    return JSON.decode(response.body);
  }

  static String root01 = "http://localhost:7170";
  static String root02 = "http://webserverredbook.azurewebsites.net";

  Future<List<Animal>> GetAnimals() async {
    var data = await GetJson(root02 + "/Data/Animals");
    List<Animal> animals = new List<Animal>();
    for (LinkedHashMap item in data) animals.add(_GetAnimal(item));
    return animals;
  }

  Future<List<AnimalType>> GetAnimalTypes() async {
    var data = await GetJson(root02 + "/Data/AnimalTypes");
    List<AnimalType> animalTypes = new List<AnimalType>();
    for (LinkedHashMap item in data) animalTypes.add(_GetAnimalType(item));
    return animalTypes;
  }

  Future<List<Image>> GetImages() async {
    var data = await GetJson(root02 + "/Data/Images");
    List<Image> images = new List<Image>();
    for (LinkedHashMap item in data) images.add(_GetImage(item));
    return images;
  }

  Future<List<ImageType>> GetImageTypes() async {
    var data = await GetJson(root02 + "/Data/ImageTypes");
    List<ImageType> imageTypes = new List<ImageType>();
    for (LinkedHashMap item in data) imageTypes.add(_GetImageType(item));
    return imageTypes;
  }

  void PostAnimal(Animal animal) {
    new BrowserClient().post(root02 + "/Data/Animals",
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(animal));
  }

  void PostAnimalType(AnimalType animalType) {
    new BrowserClient().post(root02 + "/Data/AnimalTypes",
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(animalType));
  }

  void PostImage(Image image) {
    new BrowserClient().post(root02 + "/Data/Images",
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(image));
    
  }

  void PutAnimal(Animal animal) {
    new BrowserClient().put(root02 + 
        "/Data/Animals/" + animal.id.toString(),
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(animal));
  }

  void PutAnimalType(AnimalType animalType) {
    new BrowserClient().put(root02 + 
        "/Data/AnimalTypes/" + animalType.id.toString(),
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(animalType));
  }

  void PutImage(Image image) {
    new BrowserClient().put(root02 + 
        "/Data/Images/" + image.id.toString(),
        headers: {"Content-Type": "application/json"},
        body: new God().serialize(image));
  }

  void DeleteAnimal(Animal animal) {
    new BrowserClient().delete(root02 + 
        "/Data/Animals/" + animal.id.toString());
  }

  void DeleteAnimalType(AnimalType animalType) {
    new BrowserClient().delete(root02 + "/Data/AnimalTypes/" +
        animalType.id.toString());
  }

  void DeleteImage(Image image) {
    new BrowserClient()
        .delete(root02 + "/Data/Images/" + image.id.toString());
  }

  Animal _GetAnimal(LinkedHashMap data) {
    return new Animal.FromJson(data);
  }

  Image _GetImage(LinkedHashMap data) {
    return new Image.FromJson(data);
  }

  ImageType _GetImageType(LinkedHashMap data) {
    return new ImageType.FromJson(data);
  }

  AnimalType _GetAnimalType(LinkedHashMap data) {
    return new AnimalType.FromJson(data);
  }
}
