﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Networking.Connectivity;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MobileApp.Pages;
using MobileApp.Services;

namespace MobileApp
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            SetStatusBar();
            Init();
        }

        private void SetStatusBar()
        {
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
            {

                var statusBar = StatusBar.GetForCurrentView();
                if (statusBar != null)
                {
                    statusBar.BackgroundOpacity = 1;
                    statusBar.BackgroundColor = Color.FromArgb(255,25,25, 25);
                    statusBar.ForegroundColor = Color.FromArgb(255, 247, 247, 247);
                }
            }
        }

        private void Init()
        {
            if(DataService.IsDbEmpty()) InitWithEmptyDb();
            else MainFrame.Navigate(typeof(Gallery));
        }

        private void InitWithEmptyDb()
        {
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                MainFrame.Navigate(typeof(LoadingPage));
                new Task(UpdateDb).Start();
            }
            else Frame.Navigate(typeof(ErrorPage));
        }

        private async void UpdateDb()
        {
            DataService.UpdateDb();
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Frame.Navigate(typeof(Gallery));
            });
        }
    }
}
