﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using MobileApp.Models;
using MobileApp.Services;
using Image = MobileApp.Models.Image;


namespace MobileApp.Controls
{
    public sealed partial class GalleryItem : UserControl
    {

        public GalleryItem()
        {
            this.InitializeComponent();
        }

        public int Id { get; private set; }

        public void Init(Animal animal)
        {
            SetTitle(animal.Name);
            SetShortContent(animal.ShortContent);

            Animal currentAnimal = animal;
            SetPicture(currentAnimal);

            Id = animal.Id;
        }

        private void SetPicture(Animal animal)
        {
            List<Image> images = DataService.GetImages();

            if (images.Any(x => x.AnimalId == animal.Id))
            {
                Image image = images.First(x => x.AnimalId == animal.Id);

                try
                {
                    Picture.Source = new BitmapImage()
                    {
                        UriSource = new Uri(ApplicationData.Current.LocalFolder.Path + @"\images\" + image.Path)
                    };
                }

                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
        }

        private void SetTitle(string content)
        {
            Title.Text = content;
        }

        private void SetShortContent(string content)
        {
            ShortContent.Text = content;
        }
    }
}
