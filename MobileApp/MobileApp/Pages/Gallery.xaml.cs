﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MobileApp.Models;
using MobileApp.Controls;
using MobileApp.Services;

namespace MobileApp.Pages
{
    public sealed partial class Gallery : Page
    {
        public Gallery()
        {
            this.InitializeComponent();
            Init();
        }

        private void Init()
        {
            List<Animal> animals = DataService.GetAnimals();
            InitUi(animals);
        }

        private void InitUi(List<Animal> animals)
        {
            foreach (Animal animal in animals)
               Content.Items.Add(CreateItem(animal));
        }

        private GalleryItem CreateItem(Animal animal)
        {
            GalleryItem item = new GalleryItem();
            item.Init(animal);
            item.Height = 180;
            return item;
        }

        private void Content_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NavigateToViewPage((Content.SelectedItem as GalleryItem).Id);
        }

        private void NavigateToViewPage(int id)
        {
            Frame.Navigate(typeof(ViewPage), id);
        }

        private void UpdateApp(object sender, RoutedEventArgs e)
        {
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                Frame.Navigate(typeof(LoadingPage));
                Task task = new Task(UpdateDb);
                task.Start();
            }
            else
            {
                Frame.Navigate(typeof(ErrorPage));
            }
        }

        private async void UpdateDb()
        {
            DataService.UpdateDb();
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Frame.Navigate(typeof(Gallery));
            });
        }
    }
}
