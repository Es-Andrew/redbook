﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MobileApp.Models;
using MobileApp.Services;
using Image = MobileApp.Models.Image;

namespace MobileApp.Pages
{
    public sealed partial class ViewPage : Page
    {
        public ViewPage()
        {
            this.InitializeComponent();
        }

        public int Id { get; private set; }

        private void Init(int id)
        {
            Id = id;

            Animal animal = DataService.GetAnimals().First(x => x.Id == id);
            List<Image> images = DataService.GetImages();

            if (images.Any(x => x.AnimalId == animal.Id))
                SetPicture(images.First(x => x.AnimalId == animal.Id));
            
            SetTitle(animal.Name);
            SetTextContent(animal.Content);
        }

        private void SetPicture(Image image)
        {
            Picture.Source = new BitmapImage()
            {
                UriSource = new Uri(ApplicationData.Current.LocalFolder.Path + @"\images\" + image.Path)
            };
        }

        private void SetTitle(string content)
        {
            Title.Text = content;
        }

        private void SetTextContent(string content)
        {
            TextContent.Text = content;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null) Init((int) e.Parameter);
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Gallery));
        }
    }
}
