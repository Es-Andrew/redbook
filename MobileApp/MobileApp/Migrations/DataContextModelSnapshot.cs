﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using MobileApp.Models;

namespace MobileApp.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("MobileApp.Models.Animal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AnimalTypeId");

                    b.Property<string>("Content");

                    b.Property<string>("Name");

                    b.Property<string>("ShortContent");

                    b.HasKey("Id");

                    b.HasIndex("AnimalTypeId");

                    b.ToTable("Animals");
                });

            modelBuilder.Entity("MobileApp.Models.AnimalType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AnimalTypes");
                });

            modelBuilder.Entity("MobileApp.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AnimalId");

                    b.Property<int?>("ImageTypeId");

                    b.Property<string>("Name");

                    b.Property<string>("Path");

                    b.HasKey("Id");

                    b.HasIndex("AnimalId");

                    b.HasIndex("ImageTypeId");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("MobileApp.Models.ImageType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ImageTypes");
                });

            modelBuilder.Entity("MobileApp.Models.Animal", b =>
                {
                    b.HasOne("MobileApp.Models.AnimalType", "AnimalType")
                        .WithMany("Animals")
                        .HasForeignKey("AnimalTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MobileApp.Models.Image", b =>
                {
                    b.HasOne("MobileApp.Models.Animal", "Animal")
                        .WithMany("Images")
                        .HasForeignKey("AnimalId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MobileApp.Models.ImageType", "ImageType")
                        .WithMany("Images")
                        .HasForeignKey("ImageTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
