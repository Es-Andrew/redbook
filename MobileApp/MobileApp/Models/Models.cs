﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MobileApp.Models
{
    public class DataContext : DbContext
    {
        public virtual DbSet<Animal> Animals { get; set; }
        public virtual DbSet<AnimalType> AnimalTypes { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<ImageType> ImageTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Image>()
                .HasOne(i => i.ImageType)
                .WithMany(i => i.Images)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Image>()
                .HasOne(i => i.Animal)
                .WithMany(i => i.Images)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Animal>()
                .HasOne(a => a.AnimalType)
                .WithMany(a => a.Animals)
                .OnDelete(DeleteBehavior.Cascade);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Data.db");
        }
    }

    public class Animal
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortContent { get; set; }
        public string Content { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public int? AnimalTypeId { get; set; }
        public virtual AnimalType AnimalType { get; set; }

        public Animal()
        {
            Images = new List<Image>();
        }
    }

    public class AnimalType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Animal> Animals { get; set; }

        public AnimalType()
        {
            Animals = new List<Animal>();
        }
    }

    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int? ImageTypeId { get; set; }
        public virtual ImageType ImageType { get; set; }
        public int? AnimalId { get; set; }
        public virtual Animal Animal { get; set; }
    }

    public class ImageFile : Image
    {
        public string Picture { get; set; }
    }

    public class ImageType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Image> Images { get; set; }

        public ImageType()
        {
            Images = new List<Image>();
        }
    }
}
