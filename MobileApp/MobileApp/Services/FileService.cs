﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.Storage;

namespace MobileApp.Services
{
    public class FileService
    {
        private string _root;

        public FileService(string rootPath)
        {
            _root = rootPath;
        }

        private async Task<StorageFile> CreateFile(String name)
        {
            return await ApplicationData.Current.LocalFolder.CreateFileAsync(_root + name,
                CreationCollisionOption.ReplaceExisting);
        }

        public async void SaveFile(string name, string content)
        {
            try
            {
                StorageFile file = await CreateFile(name);
                await Windows.Storage.FileIO.WriteTextAsync(file, content);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            
        }

        public async void SaveFile(string name, byte[] content)
        {
            try
            {
                StorageFile file = await CreateFile(name);
                await FileIO.WriteBytesAsync(file, content);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public async void DeleteFile(string name)
        {
            try
            {
                await (await ApplicationData.Current.LocalFolder.GetFileAsync(_root + name)).DeleteAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
           
        }

        public async void ClearDirectory()
        {
            try
            {
                await (await ApplicationData.Current.LocalFolder.GetFolderAsync(_root)).DeleteAsync();
                await ApplicationData.Current.LocalFolder.CreateFolderAsync(_root);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public async Task<string> GetFile(string name)
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(_root + name);
                return await FileIO.ReadTextAsync(file);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return "Error";
        }

        public bool IsExist(string name)
        {
            return File.Exists(Path.Combine(ApplicationData.Current.LocalFolder.Path, name));
        }
    }
}