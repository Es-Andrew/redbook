﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls.Primitives;

using Newtonsoft.Json;

using MobileApp.Models;

namespace MobileApp.Services
{
    static class WebApiService
    {
        private static string root = "http://webserverredbook.azurewebsites.net/Data/";

        private static string GetJson(string url)
        {
            string json = "";
            using (HttpClient client = new HttpClient())
            {
                json = client.GetAsync(root + url).Result.Content.ReadAsStringAsync().Result;
            }
            return json;
        }

        public static List<Animal> GetAnimals()
        {
            return JsonConvert.DeserializeObject<List<Animal>>(GetJson("Animals"));
        }

        public static List<AnimalType> GetAnimalTypes()
        {
            return JsonConvert.DeserializeObject<List<AnimalType>>(GetJson("AnimalTypes"));
        }

        public static List<ImageFile> GetImages()
        {
            return JsonConvert.DeserializeObject<List<ImageFile>>(GetJson("Images"));
        }

        public static List<ImageType> GetImageTypes()
        {
            return JsonConvert.DeserializeObject<List<ImageType>>(GetJson("ImageTypes"));
        }
    }
}
