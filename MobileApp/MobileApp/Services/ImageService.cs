﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.EntityFrameworkCore.Storage;

namespace MobileApp.Services
{
    class ImageService
    {
        public static async Task<ImageSource> GetImage(string content)
        {
            byte[] bytes;
            try
            {
                bytes = Convert.FromBase64String(content);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                bytes = new byte[0];
            }

            IRandomAccessStream image = bytes.AsBuffer().AsStream().AsRandomAccessStream();

            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(image);
            image.Seek(0);

            WriteableBitmap output = new WriteableBitmap((int)decoder.PixelHeight, (int)decoder.PixelWidth);
            await output.SetSourceAsync(image);
            return output;
        }

        public static byte[] GetBytes(string base64)
        {
            return Convert.FromBase64String(base64);
        }
    }
}
