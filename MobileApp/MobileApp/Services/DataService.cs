﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls.Primitives;
using MobileApp.Models;

namespace MobileApp.Services
{
    class DataService
    {
        private static DataContext GetContext()
        {
            return new DataContext();
        }

        public static List<Animal> GetAnimals()
        {
            return GetContext().Animals.ToList();
        }

        public static List<AnimalType> GetAnimalTypes()
        {
            return GetContext().AnimalTypes.ToList();
        }

        public static List<Image> GetImages()
        {
            return GetContext().Images.ToList();
        }

        public static List<ImageType> GetImageTypes()
        {
            return GetContext().ImageTypes.ToList();
        }

        public static void UpdateDb()
        {
            ClearDb();
            UpdateAnimalTypes();
            UpdateImageTypes();
            UpdateImages();
            UpdateAnimals();
        }

        private static void UpdateAnimals()
        {
            DataContext data = GetContext();
            IEnumerable<Animal> animals = WebApiService.GetAnimals();
            foreach (Animal animal in animals)
                data.Animals.Add(CreateValidAnimal(animal, data));
            data.SaveChanges();
        }

        private static void UpdateAnimalTypes()
        {
            DataContext data = GetContext();
            data.AnimalTypes.AddRange(WebApiService.GetAnimalTypes());
            data.SaveChanges();
        }

        private static void UpdateImages()
        {
            DataContext data = GetContext();
            IEnumerable<ImageFile> imageFiles = WebApiService.GetImages();
            SaveImageFiles(imageFiles);
            foreach (ImageFile imageFile in imageFiles)
                data.Images.Add(CreateValidImage(imageFile, data));
            data.SaveChanges();
        }

        private static void SaveImageFiles(IEnumerable<ImageFile> images)
        {
            FileService fileService = new FileService(@"images\");
            foreach (ImageFile image in images)
                fileService.SaveFile(image.Path, ImageService.GetBytes(GetValidBase64String(image.Picture)));
        }

        private static string GetValidBase64String(string content)
        {
            return content.Split(',')[1];
        }

        private static void UpdateImageTypes()
        {
            DataContext data = GetContext();
            data.ImageTypes.AddRange(WebApiService.GetImageTypes());
            data.SaveChanges();
        }

        private static Animal CreateValidAnimal(Animal animal, DataContext data)
        {
            animal.AnimalType = GetValidAnimalType(animal.AnimalType.Id, data);
            animal.Images = GetValidImages(animal.Images, data);
            return animal;
        }

        private static AnimalType GetValidAnimalType(int id, DataContext data)
        {
            return data.AnimalTypes.Find(id);
        }

        private static Image CreateValidImage(ImageFile imageFile, DataContext data)
        {
            return new Image()
            {
                Id= imageFile.Id,
                Name = imageFile.Name,
                Path = imageFile.Path,
                ImageType = GetValidImageType(imageFile.ImageType.Id, data)
            };
        }

        private static Image GetValidImage(int id, DataContext data)
        {
            return data.Images.Find(id);
        }

        private static ICollection<Image> GetValidImages(ICollection<Image> images, DataContext data)
        {
            ICollection<Image> outputImages = new List<Image>();
            foreach (Image image in images)
                outputImages.Add(GetValidImage(image.Id, data));
            return outputImages;
        }

        private static ImageType GetValidImageType(int id, DataContext data)
        {
            return data.ImageTypes.Find(id);
        }

        public static void ClearDb()
        {
            RemoveAnimals();
            RemoveAnimalTypes();
            RemoveImages();
            RemoveImageTypes();
        }

        private static void RemoveAnimals()
        {
            DataContext data = GetContext();
            IEnumerable<Animal> animals = data.Animals;
            data.Animals.RemoveRange(animals);
            data.SaveChanges();
        }

        private static void RemoveAnimalTypes()
        {
            DataContext data = GetContext();
            IEnumerable<AnimalType> animalTypes = data.AnimalTypes;
            data.AnimalTypes.RemoveRange(animalTypes);
            data.SaveChanges();
        }

        private static void RemoveImages()
        {
            DataContext data = GetContext();
            IEnumerable<Image> images = data.Images;
            data.Images.RemoveRange(images);
            //RemoveImageFiles(images);
            data.SaveChanges();
            RemoveAllImageFiles();
        }

        private static void RemoveImageFiles(IEnumerable<Image> images)
        {
            FileService fileService = new FileService(@"images\");
            foreach (Image image in images)
                fileService.DeleteFile(image.Path);
        }

        private static void RemoveAllImageFiles()
        {
            FileService fileService = new FileService(@"images");
            fileService.ClearDirectory();
        }

        private static void RemoveImageTypes()
        {
            DataContext data = GetContext();
            IEnumerable<ImageType> imageTypes = data.ImageTypes;
            data.ImageTypes.RemoveRange(imageTypes);
            data.SaveChanges();
        }

        public static bool IsDbEmpty()
        {
            DataContext data = GetContext();
            return !data.Animals.Any() && !data.AnimalTypes.Any() && !data.ImageTypes.Any() && !data.Images.Any();
        }
    }
}
